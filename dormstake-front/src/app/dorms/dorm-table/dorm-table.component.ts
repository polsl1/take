import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DormsApiService } from 'src/app/data-api/dorms/dorms-api.service';
import { DataTableColumn } from 'src/app/shared/data-table/data-table.component';
import { Dorm } from '../../data-api/dorms/dorms.model';

@Component({
  selector: 'dt-dorm-table',
  templateUrl: './dorm-table.component.html',
  styleUrls: ['./dorm-table.component.css'],
})
export class DormTableComponent implements OnInit {
  dorms: Dorm[] = [];
  currentItem: Dorm | null = null;

  readonly columns: DataTableColumn[] = [
    { field: 'name', label: 'Dorm name' },
    { field: 'street', label: 'Street' },
    { field: 'buildingNum', label: 'Building number' },
    { field: 'city', label: 'City' },
    { field: 'postalCode', label: 'Postal code' },
  ];

  dialogVisible = false;

  constructor(private dormsApi: DormsApiService, private router: Router) {}

  ngOnInit(): void {
    this.fetchDorms();
  }

  private fetchDorms() {
    this.dormsApi.getAllDorms().subscribe((dorms) => {
      this.dorms = dorms;
    });
  }

  onSubmit(dorm: Dorm) {
    this.dormsApi.postDorm(dorm).subscribe(() => {
      this.fetchDorms();
      this.dialogVisible = false;
    });
  }

  showDialog() {
    if (this.dialogVisible) return;
    this.dialogVisible = true;
  }

  onRowSelection(dorm: Dorm) {
    if (!dorm) return;
    this.router.navigate(['dorms', dorm.id!, 'rooms']);
  }
}
