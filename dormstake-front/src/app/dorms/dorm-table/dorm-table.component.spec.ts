import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DormTableComponent } from './dorm-table.component';

describe('DormTableComponent', () => {
  let component: DormTableComponent;
  let fixture: ComponentFixture<DormTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DormTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DormTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
