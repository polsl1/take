import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Student } from 'src/app/data-api/students/students.model';

@Component({
  selector: 'dt-room-details',
  templateUrl: './room-details.component.html',
  styleUrls: ['./room-details.component.css'],
})
export class RoomDetailsComponent implements OnInit, OnChanges {
  @Input() students: Student[] | null = null;

  constructor() {}

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    this.students = changes['students']?.currentValue;
  }
}
