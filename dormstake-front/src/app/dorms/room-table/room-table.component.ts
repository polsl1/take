import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OverlayPanel } from 'primeng/overlaypanel';
import { Observable, Subject } from 'rxjs';
import { DormsApiService } from 'src/app/data-api/dorms/dorms-api.service';
import { Dorm } from 'src/app/data-api/dorms/dorms.model';
import { RoomsApiService } from 'src/app/data-api/dorms/rooms-api.service';
import { Student } from 'src/app/data-api/students/students.model';
import { DataTableColumn } from 'src/app/shared/data-table/data-table.component';
import { Room } from '../../data-api/dorms/dorms.model';
import { DormsPaths } from '../dorms-routing.module';

type RoomStudents = {
  [id: number]: Student[];
};

@Component({
  selector: 'dt-room-table',
  templateUrl: './room-table.component.html',
  styleUrls: ['./room-table.component.css'],
})
export class RoomTableComponent implements OnInit {
  dorm?: Dorm;
  get dormName(): string {
    return this.dorm ? this.dorm.name : 'Dorm';
  }

  rooms: Room[] = [];
  selectedRoom?: Room;

  roomStudents: RoomStudents = {};

  readonly columns: DataTableColumn[] = [
    { label: 'Number', field: 'number' },
    { label: 'Floor', field: 'floor' },
    { label: 'Maximum capacity', field: 'capacity' },
    { label: 'Area', field: 'area' },
  ];

  dialogVisible = false;

  dormEdit?: Dorm;

  constructor(
    private route: ActivatedRoute,
    private dormsApi: DormsApiService,
    private roomsApi: RoomsApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.fetchDormWithRooms(params['dormId']);
    });
  }

  private fetchDormWithRooms(dormId: number) {
    this.dormsApi.getDormWithRooms(dormId).subscribe((roomList) => {
      this.dorm = roomList.dorm;
      this.rooms = roomList.rooms;
      this.roomStudents = {};

      this.dormEdit = { ...this.dorm };

      if (this.selectedRoom) this.fetchStudents(this.selectedRoom.id!);
    });
  }

  editDorm(dorm: Dorm) {
    dorm.id = this.dorm!.id!;
    this.dormsApi
      .putDorm(dorm)
      .subscribe(() => this.fetchDormWithRooms(this.dorm!.id!));
  }

  removeDorm() {
    this.dormsApi.getDormWithRooms(this.dorm!.id!).subscribe((roomList) => {
      this.removeAllRooms(roomList.rooms).subscribe((success) => {
        if (success) {
          this.dormsApi.deleteDorm(this.dorm!.id!).subscribe(() => {
            this.router.navigate([DormsPaths.home]);
          });
        }
      });
    });
  }

  private removeAllRooms(rooms: Room[]): Observable<boolean> {
    let counter = rooms.length;
    if (counter === 0) return new Observable((sub) => sub.next(true));

    const finished = new Subject<boolean>();
    rooms.forEach((room) => {
      this.roomsApi.deleteRoom(room.id!).subscribe(() => {
        counter--;
        if (counter === 0) {
          finished.next(true);
        }
      });
    });

    return finished.asObservable();
  }

  submitRoom(room: Room) {
    room = { ...room, dorm: this.dorm! };
    this.roomsApi.postRoom(room).subscribe(() => {
      this.fetchDormWithRooms(this.dorm!.id!);
      this.dialogVisible = false;
    });
  }

  showDialog() {
    if (this.dialogVisible) return;
    this.dialogVisible = true;
  }

  removeRoom() {
    if (!this.selectedRoom) return;
    this.roomsApi
      .deleteRoom(this.selectedRoom.id!)
      .subscribe(() => this.fetchDormWithRooms(this.dorm!.id!));
  }

  editRoom(room: Room) {
    room.dorm = this.dorm!;
    this.roomsApi
      .putRoom(room)
      .subscribe(() => this.fetchDormWithRooms(this.dorm!.id!));
  }

  onRoomSelection(room: Room) {
    if (!room || this.roomStudents[room.id!]) return;
    this.fetchStudents(room.id!);
  }

  private fetchStudents(roomId: number) {
    this.roomsApi
      .getStudentsFromRoom(roomId)
      .subscribe((data) => (this.roomStudents[data.room.id!] = data.students));
  }

  getStudentsFromSelection() {
    if (!this.selectedRoom) return null;
    return this.roomStudents[this.selectedRoom.id!] ?? [];
  }
}
