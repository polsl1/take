import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dt-dorms',
  template: '<dt-dorm-table></dt-dorm-table>'
})
export class DormsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
