import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ErrorMessagesDict, ValidationFormComponent } from 'src/app/shared/validation-form/validation-form.component';
import { Room } from '../../../data-api/dorms/dorms.model';

@Component({
  selector: 'dt-room-form',
  templateUrl: './room-form.component.html',
  styleUrls: ['./room-form.component.css'],
})
export class RoomFormComponent
  extends ValidationFormComponent
  implements OnInit, OnChanges {

  @Input() model?: Room;

  @Input() submitLabel = 'Submit';

  @Output() onSubmit = new EventEmitter<Room>();

  fieldNames = {
    number: 'number',
    floor: 'floor',
    capacity: 'capacity',
    area: 'area'
  } as const

  override form = new FormGroup({
    [this.fieldNames.number]: new FormControl(null, {
      validators: [Validators.required, Validators.pattern(/\d{1,3}/)],
    }),
    [this.fieldNames.floor]: new FormControl(null, {
      validators: [Validators.required, Validators.pattern(/\d{1,3}/)],
    }),
    [this.fieldNames.capacity]: new FormControl(null, {
      validators: [Validators.required, Validators.pattern(/\d{1,3}/)],
    }),
    [this.fieldNames.area]: new FormControl(null, {
      validators: [Validators.required, Validators.pattern(/\d{1,3}/)],
    }),
  });

  override errorMessages: ErrorMessagesDict = {
    [this.fieldNames.number]: {
      required: 'Required',
      pattern: 'Invalid pattern'
    },
    [this.fieldNames.floor]: {
      required: 'Required',
      pattern: 'Invalid pattern'
    },
    [this.fieldNames.capacity]: {
      required: 'Required',
      pattern: 'Invalid pattern'
    },
    [this.fieldNames.area]: {
      required: 'Required',
      pattern: 'Invalid pattern'
    },
  }

  constructor() {
    super();
  }

  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges): void {
    const currentVal = changes['model']?.currentValue
    if (!currentVal) return
    this.model = currentVal
    this.form?.patchValue(currentVal);
  }

  submit() {
    if (this.form.valid) {
      const emitVal = this.form.value
      emitVal.id = this.model?.id
      this.onSubmit.emit(emitVal);
    }
  }
}
