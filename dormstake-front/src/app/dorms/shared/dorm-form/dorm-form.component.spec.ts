import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DormFormComponent } from './dorm-form.component';

describe('DormFormComponent', () => {
  let component: DormFormComponent;
  let fixture: ComponentFixture<DormFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DormFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DormFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
