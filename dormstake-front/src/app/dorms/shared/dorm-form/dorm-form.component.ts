import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Dorm } from 'src/app/data-api/dorms/dorms.model';
import {
  ErrorMessagesDict,
  ValidationFormComponent,
} from 'src/app/shared/validation-form/validation-form.component';

const enum DormFormFields {
  Name = 'name',
  Street = 'street',
  BuildingNum = 'buildingNum',
  City = 'city',
  PostalCode = 'postalCode',
}

@Component({
  selector: 'dt-dorm-form',
  templateUrl: './dorm-form.component.html',
  styleUrls: ['./dorm-form.component.css'],
})
export class DormFormComponent
  extends ValidationFormComponent
  implements OnInit, OnChanges {
  @Input() model?: Dorm;

  @Input() submitLabel = 'Submit';

  @Output() onSubmit = new EventEmitter<Dorm>();

  override form = new FormGroup({
    [DormFormFields.Name]: new FormControl(null, {
      validators: [Validators.required],
    }),

    [DormFormFields.Street]: new FormControl(null, {
      validators: [Validators.required],
    }),

    [DormFormFields.BuildingNum]: new FormControl(null, {
      validators: [Validators.required],
    }),

    [DormFormFields.City]: new FormControl(null, {
      validators: [Validators.required],
    }),

    [DormFormFields.PostalCode]: new FormControl(null, {
      validators: [Validators.required],
    }),
  });

  override errorMessages: ErrorMessagesDict = {
    [DormFormFields.Name]: {
      required: 'Dorm name is required',
    },
    [DormFormFields.Street]: {
      required: 'Required',
    },
    [DormFormFields.BuildingNum]: {
      required: 'Required',
    },
    [DormFormFields.City]: {
      required: 'Required',
    },
    [DormFormFields.PostalCode]: {
      required: 'Required',
    },
  };

  constructor() {
    super();
  }

  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges): void {
    const currentVal = changes['model']?.currentValue
    if (!currentVal) return
    this.model = currentVal
    this.form?.patchValue(currentVal);
  }

  submit() {
    this.submitAttempt = true;
    if (this.form.valid) {
      const emitVal = this.form.value
      emitVal.id = this.model?.id
      this.onSubmit.emit(emitVal);
    }
  }
}
