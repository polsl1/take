import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoomTableComponent } from './room-table/room-table.component';

export const DormsPaths = {
  home: 'dorms',
  rooms: 'dorms/:dormId/rooms',
} as const;

const routes: Routes = [
  { path: DormsPaths.rooms, component: RoomTableComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DormsRoutingModule {}
