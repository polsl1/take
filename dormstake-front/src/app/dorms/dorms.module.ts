import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';

import { DormsRoutingModule } from './dorms-routing.module';
import { DormTableComponent } from './dorm-table/dorm-table.component';
import { DormFormComponent } from './shared/dorm-form/dorm-form.component';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from 'primeng/tooltip';
import { RoomTableComponent } from './room-table/room-table.component';
import { RoomFormComponent } from './shared/room-form/room-form.component';
import { SharedModule } from '../shared/shared.module';
import { DormsComponent } from './dorms.component';
import { PanelModule } from 'primeng/panel';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ToolbarModule } from 'primeng/toolbar';
import { CardModule } from 'primeng/card';
import { DividerModule } from 'primeng/divider';
import { ReactiveFormsModule } from '@angular/forms';
import { RoomDetailsComponent } from './room-details/room-details.component';

@NgModule({
  declarations: [
    DormTableComponent,
    DormFormComponent,
    RoomFormComponent,
    DormsComponent,
    RoomTableComponent,
    RoomDetailsComponent,
  ],
  imports: [
    CommonModule,
    DormsRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    SharedModule,

    ButtonModule,
    InputMaskModule,
    InputTextModule,
    InputNumberModule,
    TooltipModule,
    DialogModule,
    PanelModule,
    OverlayPanelModule,
    CardModule,
    ToolbarModule,
    DividerModule,
  ],
  exports: [DormsComponent],
})
export class DormsModule { }
