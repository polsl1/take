import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AppPaths } from './app-routing.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'dormstake';

  readonly menuItems: MenuItem[] = [
    { label: 'Dorms', routerLink: AppPaths.Dorms, icon: 'pi pi-home' },
    { label: 'Students', routerLink: AppPaths.Students, icon: 'pi pi-users' }
  ]
}
