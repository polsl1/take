import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  static readonly BASE_URL = 'http://localhost:8080/dormstake';

  constructor(protected readonly http: HttpClient) {}

  public get httpClient(): HttpClient {
    return this.http;
  }
}
