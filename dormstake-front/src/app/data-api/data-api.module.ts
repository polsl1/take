import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './api.service';
import { DormsApiService } from './dorms/dorms-api.service';
import { RoomsApiService } from './dorms/rooms-api.service';
import { AccommodationsApiService } from './students/accommodations-api.service';
import { ChargesApiService } from './students/charges-api.service';
import { StudentsApiService } from './students/students-api.service';

@NgModule({
  imports: [CommonModule, HttpClientModule],
  providers: [
    ApiService,
    DormsApiService,
    RoomsApiService,
    AccommodationsApiService,
    StudentsApiService,
    ChargesApiService,
  ],
})
export class DataApiModule {}
