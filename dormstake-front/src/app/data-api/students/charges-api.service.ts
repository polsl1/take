import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Accommodation, Charge } from './students.model';

@Injectable({
  providedIn: 'root',
})
export class ChargesApiService extends ApiService {
  static readonly API_URL = ApiService.BASE_URL + '/charges';

  constructor(http: HttpClient) {
    super(http);
  }

  getCharge(chargeId: number): Observable<Charge> {
    return this.http.get<Charge>(`${ChargesApiService.API_URL}/${chargeId}`, {
      observe: 'body',
    });
  }

  postCharge(charge: Charge) {
    return this.http.post(ChargesApiService.API_URL, charge, {
      observe: 'response',
    });
  }

  deleteCharge(chargeId: number) {
    return this.http.delete(`${ChargesApiService.API_URL}/${chargeId}`);
  }

  putCharge(charge: Charge) {
    return this.http.put(ChargesApiService.API_URL, charge);
  }
}
