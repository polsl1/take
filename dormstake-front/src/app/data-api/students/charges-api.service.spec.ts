import { TestBed } from '@angular/core/testing';

import { ChargesApiService } from './charges-api.service';

describe('ChargesApiService', () => {
  let service: ChargesApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ChargesApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
