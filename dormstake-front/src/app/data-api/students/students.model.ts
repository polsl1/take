import { Room } from '../dorms/dorms.model';

export interface Student {
  id?: number;
  firstName: string;
  lastName: string;
  albumNum: number;
  telNum: number;
}

export interface Accommodation {
  id?: number;
  student: Student;
  room: Room;
  dateFrom: Date;
  dateTo?: Date;
}

export interface AccommodationList {
  student: Student;
  accommodations: Accommodation[];
}

export interface Charge {
  id?: number;
  accommodation: number;
  postedDate: Date;
  dueDate: Date;
  paidDate: Date;
  amount: number;
}

export interface ChargeList {
  accommodation: Accommodation;
  charges: Charge[];
}
