import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Accommodation, ChargeList } from './students.model';

@Injectable({
  providedIn: 'root',
})
export class AccommodationsApiService extends ApiService {
  static readonly API_URL = ApiService.BASE_URL + '/accommodations';

  constructor(http: HttpClient) {
    super(http);
  }

  getAccommodation(accommodationId: number): Observable<Accommodation> {
    return this.http.get<Accommodation>(
      `${AccommodationsApiService.API_URL}/${accommodationId}`,
      { observe: 'body' }
    );
  }

  postAccommodation(accommodation: Accommodation) {
    return this.http.post(AccommodationsApiService.API_URL, accommodation, {
      observe: 'response',
    });
  }

  deleteAccommodation(accommodationId: number) {
    return this.http.delete(
      `${AccommodationsApiService.API_URL}/${accommodationId}`
    );
  }

  putAccommodation(accommodation: Accommodation) {
    return this.http.put(AccommodationsApiService.API_URL, accommodation);
  }

  getAccommodationWithCharges(accommodationId: number): Observable<ChargeList> {
    return this.http.get<ChargeList>(
      `${AccommodationsApiService.API_URL}/${accommodationId}/charges`,
      { observe: 'body' }
    );
  }
}
