import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {
  AccommodationList,
  Student,
} from 'src/app/data-api/students/students.model';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root',
})
export class StudentsApiService extends ApiService {
  static readonly API_URL = ApiService.BASE_URL + '/students';

  constructor(http: HttpClient) {
    super(http);
  }

  getAllStudents(): Observable<Student[]> {
    return this.http.get<Student[]>(StudentsApiService.API_URL, {
      observe: 'body',
    });
  }

  getStudent(studentId: number): Observable<Student> {
    return this.http.get<Student>(
      `${StudentsApiService.API_URL}/${studentId}`,
      {
        observe: 'body',
      }
    );
  }

  deleteStudent(studentId: number): Observable<any> {
    return this.http.delete(`${StudentsApiService.API_URL}/${studentId}`);
  }

  postStudent(student: Student): Observable<any> {
    return this.http.post(StudentsApiService.API_URL, student, {
      observe: 'response',
    });
  }

  putStudent(student: Student): Observable<any> {
    return this.http.put(StudentsApiService.API_URL, student, {
      observe: 'response',
    });
  }

  getStudentWithAccommodations(
    studentId: number
  ): Observable<AccommodationList> {
    return this.http.get<AccommodationList>(
      `${StudentsApiService.API_URL}/${studentId}/accommodations`,
      { observe: 'body' }
    );
  }
}
