import { TestBed } from '@angular/core/testing';

import { AccommodationsApiService } from './accommodations-api.service';

describe('AccommodationsApiService', () => {
  let service: AccommodationsApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccommodationsApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
