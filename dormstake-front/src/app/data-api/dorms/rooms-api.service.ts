import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from '../api.service';
import { Dorm, Room, RoomStudentsList } from './dorms.model';

@Injectable({
  providedIn: 'root',
})
export class RoomsApiService extends ApiService {
  static readonly API_URL = ApiService.BASE_URL + '/rooms';

  constructor(http: HttpClient) {
    super(http);
  }

  getAllRooms(dormId: number): Observable<Room[]> {
    return this.http.get<Room[]>(RoomsApiService.API_URL, {
      params: { dormId },
      observe: 'body',
    });
  }

  getRoom(roomId: number): Observable<Room> {
    return this.http.get<Room>(`${RoomsApiService.API_URL}/${roomId}`, {
      observe: 'body',
    });
  }

  postRoom(room: Room) {
    return this.http.post(RoomsApiService.API_URL, room, {
      observe: 'response',
    });
  }

  deleteRoom(roomId: number) {
    return this.http.delete(`${RoomsApiService.API_URL}/${roomId}`);
  }

  putRoom(room: Room) {
    return this.http.put(RoomsApiService.API_URL, room);
  }

  getStudentsFromRoom(roomId: number): Observable<RoomStudentsList> {
    return this.http.get<RoomStudentsList>(`${RoomsApiService.API_URL}/${roomId}/students`)
  }
}
