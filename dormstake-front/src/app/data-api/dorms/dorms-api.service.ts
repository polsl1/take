import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from 'src/app/data-api/api.service';
import { Dorm, RoomList } from 'src/app/data-api/dorms/dorms.model';

@Injectable({
  providedIn: 'root',
})
export class DormsApiService extends ApiService {
  static readonly API_URL = ApiService.BASE_URL + '/dorms';

  constructor(http: HttpClient) {
    super(http);
  }

  getAllDorms(): Observable<Dorm[]> {
    return this.http.get<Dorm[]>(DormsApiService.API_URL, { observe: 'body' });
  }

  getDorm(dormId: number): Observable<Dorm> {
    return this.http.get<Dorm>(`${DormsApiService.API_URL}/${dormId}`, {
      observe: 'body',
    });
  }

  deleteDorm(dormId: number): Observable<any> {
    return this.http.delete(`${DormsApiService.API_URL}/${dormId}`);
  }

  postDorm(dorm: Dorm): Observable<any> {
    return this.http.post(DormsApiService.API_URL, dorm, {
      observe: 'response',
    });
  }

  putDorm(dorm: Dorm): Observable<any> {
    return this.http.put(DormsApiService.API_URL, dorm, {
      observe: 'response',
    });
  }

  getDormWithRooms(dormId: number): Observable<RoomList> {
    return this.http.get<RoomList>(
      `${DormsApiService.API_URL}/${dormId}/rooms`,
      { observe: 'body' }
    );
  }
}
