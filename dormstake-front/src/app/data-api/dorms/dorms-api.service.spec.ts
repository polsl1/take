import { TestBed } from '@angular/core/testing';

import { DormsApiService } from './dorms-api.service';

describe('DormsApiService', () => {
  let service: DormsApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DormsApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
