import { Student } from '../students/students.model';

export interface Dorm {
  id?: number;
  name: string;
  street: string;
  city: string;
  buildingNum: number;
  postalCode: string;
}

export interface Room {
  id?: number;
  number: number;
  capacity: number;
  area: number;
  floor: number;
  dorm: Dorm;
}

export interface RoomList {
  dorm: Dorm;
  rooms: Room[];
}

export interface RoomStudentsList {
  room: Room;
  students: Student[];
}
