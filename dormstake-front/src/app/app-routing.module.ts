import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DormsComponent } from './dorms/dorms.component';
import { StudentsComponent } from './students/students.component';

export enum AppPaths {
  Home = '',
  Dorms = 'dorms',
  Students = 'students'
}

const routes: Routes = [
  { path: AppPaths.Home, redirectTo: AppPaths.Dorms, pathMatch: 'full' },
  { path: AppPaths.Dorms, component: DormsComponent },
  { path: AppPaths.Students, component: StudentsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
