import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataTableComponent } from './data-table/data-table.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { ButtonModule } from 'primeng/button';
import { RouterModule } from '@angular/router';
import { TableModule } from 'primeng/table';
import { PanelModule } from 'primeng/panel';
import { ErrorDisplayComponent } from './error-display/error-display.component';
import { TooltipModule } from 'primeng/tooltip';
import { ValidationFormComponent } from './validation-form/validation-form.component';


@NgModule({
  declarations: [DataTableComponent, SideMenuComponent, ErrorDisplayComponent, ValidationFormComponent],
  imports: [
    CommonModule,
    RouterModule,

    TableModule,
    PanelModule,
    TooltipModule,
    ButtonModule,
  ],
  exports: [DataTableComponent, SideMenuComponent, ErrorDisplayComponent, ValidationFormComponent]
})
export class SharedModule { }
