import { Component } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ErrorMessages, FieldErrors } from '../error-display/error-display.model';

export type ErrorMessagesDict = { [name: string]: ErrorMessages }

@Component({
  selector: 'dt-validation-form',
  template: ''
})
export class ValidationFormComponent {

  submitAttempt = false

  form = new FormGroup({})

  errorMessages: ErrorMessagesDict = {}

  constructor() { }

  getFieldErrors(fieldName: string): FieldErrors {
    const field = this.form.get(fieldName)
    if (!field) {
      return {
        errors: null,
        messages: {}
      }
    }

    return {
      errors: field.errors,
      messages: this.errorMessages[fieldName]
    }
  }

  isFieldValid(fieldName: string) {
    const field = this.form.get(fieldName)
    if (!field) return true

    return (
      (field.valid && field.untouched) ||
      (field.untouched && !this.submitAttempt)
    )
  }

  applyValidationStyle(field: string) {
    const invalid = !this.isFieldValid(field)
    return {
      'has-error': invalid,
      'has-feedback': invalid,
    };
  }

  reset() {
    this.form.reset();
    this.submitAttempt = false;
  }

}
