import { ValidationErrors } from '@angular/forms';

export interface FieldErrors {
  errors: ValidationErrors | null;
  messages: ErrorMessages;
}

export type ErrorMessages = {
  [key: string]: string
}
