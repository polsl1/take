import { Component, Input, OnInit } from '@angular/core';
import { FieldErrors } from './error-display.model';

@Component({
  selector: 'error-display',
  templateUrl: './error-display.component.html',
  styleUrls: ['./error-display.component.css'],
})
export class ErrorDisplayComponent implements OnInit {

  @Input() errorData?: FieldErrors;
  @Input() displayError: boolean = false;

  ngOnInit() {
  }

  getErrorMessage(errorCode: string) {
    if (!this.errorData?.errors) return ''
    const msg = this.errorData.messages[errorCode]

    return msg ?? ''
  }
}
