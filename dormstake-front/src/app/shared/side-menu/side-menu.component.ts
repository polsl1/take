import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'dt-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {

  @Input() styleClass: string = ''

  @Input() items: MenuItem[] = []

  constructor() { }

  ngOnInit(): void {
  }

}
