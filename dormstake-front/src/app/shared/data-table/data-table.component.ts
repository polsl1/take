import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
} from '@angular/core';

export interface DataTableColumn {
  label: string;
  field?: string;
  map?: (rowData: any) => string;
}

@Component({
  selector: 'dt-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.css'],
})
export class DataTableComponent implements OnInit {
  @Input() columns: DataTableColumn[] = [];
  @Input() data: any[] = [];
  @Input() header: string = '';

  @Input() headerMenu: TemplateRef<any> | null = null;

  @Input() dataKey: string = '';

  @Input() selectable = false;
  @Input() selection: any | null = null;
  @Output() selectionChange = new EventEmitter<any>();

  constructor() {}

  ngOnInit(): void {}

  onTableSelectionChange(selection: any) {
    this.selection = selection;
    this.selectionChange.emit(selection);
  }

  renderField(rowData: any, column: DataTableColumn): string {
    if (column.field) return rowData[column.field];

    if (column.map) return column.map(rowData);

    return '';
  }
}
