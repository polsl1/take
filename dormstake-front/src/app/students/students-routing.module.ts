import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccommodationTableComponent } from './accommodations-table/accommodations-table.component';
import { ChargesTableComponent } from './charges-table/charges-table.component';

export enum StudentsPaths {
  home = 'students',
  accommodations = 'students/:studentId/accommodations',
  charges = 'students/:studentId/accommodations/:accommodationId/charges',
}

const routes: Routes = [
  {
    path: StudentsPaths.accommodations,
    component: AccommodationTableComponent,
  },
  {
    path: StudentsPaths.charges,
    component: ChargesTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentsRoutingModule {}
