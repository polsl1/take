import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DormsApiService } from 'src/app/data-api/dorms/dorms-api.service';
import { Dorm, Room } from 'src/app/data-api/dorms/dorms.model';
import { RoomsApiService } from 'src/app/data-api/dorms/rooms-api.service';
import { Accommodation } from 'src/app/data-api/students/students.model';
import { ValidationFormComponent } from 'src/app/shared/validation-form/validation-form.component';

const enum AccommodationFormFields {
  DateFrom = 'dateFrom',
  DateTo = 'dateTo',
  Dorm = 'dorm',
  Room = 'room',
}

@Component({
  selector: 'dt-accommodation-form',
  templateUrl: './accommodation-form.component.html',
  styleUrls: ['./accommodation-form.component.css'],
})
export class AccommodationFormComponent
  extends ValidationFormComponent
  implements OnInit, OnChanges
{
  @Input() model?: Accommodation;

  @Input() submitLabel = 'Submit';

  @Output() onSubmit = new EventEmitter<Accommodation>();

  dorms: Dorm[] = [];
  rooms: Room[] = [];

  constructor(
    private dormsApi: DormsApiService,
    private roomsApi: RoomsApiService
  ) {
    super();
  }

  override form = new FormGroup({
    [AccommodationFormFields.DateFrom]: new FormControl(null, {
      validators: [Validators.required],
    }),
    [AccommodationFormFields.DateTo]: new FormControl(null, {
      validators: [],
    }),
    [AccommodationFormFields.Dorm]: new FormControl(null, {
      validators: [Validators.required],
    }),
    [AccommodationFormFields.Room]: new FormControl(null, {
      validators: [Validators.required],
    }),
  });

  override errorMessages = {
    [AccommodationFormFields.DateFrom]: {
      required: 'Required',
    },
    [AccommodationFormFields.DateTo]: {
      required: 'Required',
    },
    [AccommodationFormFields.Dorm]: {
      required: 'Required',
    },
    [AccommodationFormFields.Room]: {
      required: 'Required',
    },
  };

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    const currentVal = changes['model']?.currentValue;
    if (!currentVal) return;
    this.model = currentVal;
    this.form?.patchValue(currentVal);
  }

  getDropdownDorms = () => {
    this.dormsApi.getAllDorms().subscribe((dorms) => {
      this.dorms = dorms;
    });
  };

  getDropdownRooms = () => {
    this.roomsApi.getAllRooms(this.form.value['dorm'].id).subscribe((rooms) => {
      this.rooms = rooms;
    });
  };

  submit() {
    this.submitAttempt = true;
    if (this.form.valid) {
      this.form.get([AccommodationFormFields.Dorm])?.disable();
      const emitVal = this.form.value;
      emitVal.id = this.model?.id;
      this.onSubmit.emit(emitVal);
      this.form.get([AccommodationFormFields.Dorm])?.enable();
    }
  }
}
