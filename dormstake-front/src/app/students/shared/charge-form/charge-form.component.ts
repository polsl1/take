import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DormsApiService } from 'src/app/data-api/dorms/dorms-api.service';
import { Dorm, Room } from 'src/app/data-api/dorms/dorms.model';
import { RoomsApiService } from 'src/app/data-api/dorms/rooms-api.service';
import {
  Accommodation,
  Charge,
} from 'src/app/data-api/students/students.model';
import { ValidationFormComponent } from 'src/app/shared/validation-form/validation-form.component';

const enum ChargeFormFields {
  DueDate = 'dueDate',
  PaidDate = 'paidDate',
  Amount = 'amount',
}

@Component({
  selector: 'dt-charge-form',
  templateUrl: './charge-form.component.html',
  styleUrls: ['./charge-form.component.css'],
})
export class ChargeFormComponent
  extends ValidationFormComponent
  implements OnInit, OnChanges
{
  @Input() model?: Charge;

  @Input() submitLabel = 'Submit';

  @Output() onSubmit = new EventEmitter<Charge>();

  constructor() {
    super();
  }

  override form = new FormGroup({
    [ChargeFormFields.DueDate]: new FormControl(null, {
      validators: [Validators.required],
    }),
    [ChargeFormFields.PaidDate]: new FormControl(null, {
      validators: [],
    }),
    [ChargeFormFields.Amount]: new FormControl(null, {
      validators: [Validators.required],
    }),
  });

  override errorMessages = {
    [ChargeFormFields.PaidDate]: {
      required: 'Required',
    },
    [ChargeFormFields.DueDate]: {
      required: 'Required',
    },
    [ChargeFormFields.Amount]: {
      required: 'Required',
    },
  };

  ngOnInit(): void {}

  ngOnChanges(changes: SimpleChanges): void {
    const currentVal = changes['model']?.currentValue;
    if (!currentVal) return;
    this.model = { ...currentVal };
    this.form?.patchValue(currentVal);
    this.form?.patchValue({ amount: (this.model!.amount / 100).toString() });
  }

  submit() {
    this.submitAttempt = true;
    if (this.form.valid) {
      const emitVal = this.form.value;
      emitVal.postedDate = new Date();
      emitVal.id = this.model?.id;
      emitVal.amount = emitVal.amount * 100;
      this.onSubmit.emit(emitVal);
    }
  }
}
