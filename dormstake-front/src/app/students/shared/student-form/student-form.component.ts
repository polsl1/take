import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ValidationFormComponent } from 'src/app/shared/validation-form/validation-form.component';
import { Student } from '../../../data-api/students/students.model';

const enum StudentFormFields {
  FirstName = 'firstName',
  LastName = 'lastName',
  AlbumNum = 'albumNum',
  TelNum = 'telNum',
}

@Component({
  selector: 'dt-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css'],
})
export class StudentFormComponent
  extends ValidationFormComponent
  implements OnInit, OnChanges {
  @Input() submitLabel = 'Submit';
  @Input() model?: Student;

  @Output() onSubmitEvent = new EventEmitter<Student>();

  override form = new FormGroup({
    [StudentFormFields.FirstName]: new FormControl(null, {
      validators: [Validators.required],
    }),

    [StudentFormFields.LastName]: new FormControl(null, {
      validators: [Validators.required],
    }),

    [StudentFormFields.AlbumNum]: new FormControl(null, {
      validators: [Validators.required, Validators.pattern(/\d{6}/)],
    }),

    [StudentFormFields.TelNum]: new FormControl(null, {
      validators: [Validators.required, Validators.pattern(/\d{9}/)],
    }),
  });

  override errorMessages = {
    [StudentFormFields.FirstName]: {
      required: 'Required',
    },
    [StudentFormFields.LastName]: {
      required: 'Required',
    },
    [StudentFormFields.AlbumNum]: {
      required: 'Required',
      pattern: 'Invalid album number',
    },
    [StudentFormFields.TelNum]: {
      required: 'Required',
      pattern: 'Invalid telephone number',
    },
  };

  constructor() {
    super();
  }

  ngOnInit(): void { }

  ngOnChanges(changes: SimpleChanges): void {
    const currentVal = changes['model']?.currentValue
    if (!currentVal) return
    this.model = currentVal
    this.form?.patchValue(currentVal);
  }

  onSubmit() {
    this.submitAttempt = true;
    if (this.form.valid) {
      const emitVal = this.form.value
      emitVal.id = this.model?.id
      this.onSubmitEvent.emit(emitVal);
    }
  }
}
