import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OverlayPanel } from 'primeng/overlaypanel';
import { Observable, Subject } from 'rxjs';
import { StudentsApiService } from 'src/app/data-api/students/students-api.service';
import {
  Accommodation,
  Student,
} from 'src/app/data-api/students/students.model';
import { AccommodationsApiService } from 'src/app/data-api/students/accommodations-api.service';
import { DataTableColumn } from 'src/app/shared/data-table/data-table.component';
import { StudentsPaths } from '../students-routing.module';

@Component({
  selector: 'dt-accommodation-table',
  templateUrl: './accommodations-table.component.html',
  styleUrls: ['./accommodations-table.component.css'],
})
export class AccommodationTableComponent implements OnInit {
  student?: Student;
  get studentName(): string {
    return this.student
      ? `${this.student.firstName} ${this.student.lastName}`
      : 'Student';
  }

  accommodations: Accommodation[] = [];
  tableData: any[] = [];

  readonly columns: DataTableColumn[] = [
    { label: 'Date from', field: 'dateFrom' },
    { label: 'Date to', field: 'dateTo' },
    { label: 'Dorm', field: 'dorm' },
    { label: 'Room', field: 'room' },
  ];

  dialogVisible = false;

  studentEdit?: Student;

  @ViewChild('editAccommodationOp') editAccommodationOp!: OverlayPanel;

  constructor(
    private route: ActivatedRoute,
    private studentsApi: StudentsApiService,
    private accommodationsApi: AccommodationsApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.fetchStudentWithAccommodations(params['studentId']);
    });
  }

  private fetchStudentWithAccommodations(studentId: number) {
    this.studentsApi
      .getStudentWithAccommodations(studentId)
      .subscribe((accommodationList) => {
        this.student = accommodationList.student;
        this.accommodations = accommodationList.accommodations;

        this.tableData = this.accommodations.map((accommodation) => ({
          ...accommodation,
          dorm: accommodation.room.dorm.name,
          room: accommodation.room.number,
        }));
        this.studentEdit = { ...this.student };
      });
  }

  editStudent(student: Student) {
    student.id = this.student!.id!;
    this.studentsApi
      .putStudent(student)
      .subscribe(() => this.fetchStudentWithAccommodations(this.student!.id!));
  }

  removeStudent() {
    this.studentsApi
      .getStudentWithAccommodations(this.student!.id!)
      .subscribe((accommodationList) => {
        this.removeAllAccommodations(
          accommodationList.accommodations
        ).subscribe((success) => {
          if (success) {
            this.studentsApi.deleteStudent(this.student!.id!).subscribe(() => {
              this.router.navigate([StudentsPaths.home]);
            });
          }
        });
      });
  }

  private removeAllAccommodations(
    accommodations: Accommodation[]
  ): Observable<boolean> {
    let counter = accommodations.length;
    if (counter === 0) return new Observable((sub) => sub.next(true));

    const finished = new Subject<boolean>();
    accommodations.forEach((accommodation) => {
      this.accommodationsApi
        .deleteAccommodation(accommodation.id!)
        .subscribe(() => {
          counter--;
          if (counter === 0) {
            finished.next(true);
          }
        });
    });

    return finished.asObservable();
  }

  submitAccommodation(accommodation: Accommodation) {
    accommodation.student = this.student!;
    this.accommodationsApi.postAccommodation(accommodation).subscribe(() => {
      this.fetchStudentWithAccommodations(this.student!.id!);
      this.dialogVisible = false;
    });
  }

  showDialog() {
    if (this.dialogVisible) return;
    this.dialogVisible = true;
  }

  onRowSelection(accommodation: Accommodation) {
    if (!accommodation) return;
    this.router.navigate([
      'students',
      accommodation.student.id,
      'accommodations',
      accommodation.id!,
      'charges',
    ]);
  }
}
