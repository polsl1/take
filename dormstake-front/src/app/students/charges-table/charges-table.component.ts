import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { OverlayPanel } from 'primeng/overlaypanel';
import { Observable, Subject } from 'rxjs';
import { StudentsApiService } from 'src/app/data-api/students/students-api.service';
import {
  Accommodation,
  Charge,
} from 'src/app/data-api/students/students.model';
import { AccommodationsApiService } from 'src/app/data-api/students/accommodations-api.service';
import { DataTableColumn } from 'src/app/shared/data-table/data-table.component';
import { StudentsPaths } from '../students-routing.module';
import { ChargesApiService } from 'src/app/data-api/students/charges-api.service';

@Component({
  selector: 'dt-charges-table',
  templateUrl: './charges-table.component.html',
  styleUrls: ['./charges-table.component.css'],
})
export class ChargesTableComponent implements OnInit {
  accommodation?: Accommodation;
  get accommodationName(): string {
    if (!this.accommodation) return 'Accommodation';
    return this.accommodation.dateTo
      ? `${this.accommodation.room.dorm.name} ${this.accommodation.room.number}: ${this.accommodation.dateFrom} - ${this.accommodation.dateTo}`
      : `${this.accommodation.room.dorm.name} ${this.accommodation.room.number}: ${this.accommodation.dateFrom}`;
  }

  charges: Charge[] = [];
  selectedCharge?: Charge;

  backLink = `/students`;

  readonly columns: DataTableColumn[] = [
    { label: 'Post date', field: 'postedDate' },
    { label: 'Due date', field: 'dueDate' },
    { label: 'Paid date', field: 'paidDate' },
    { label: 'Amount', map: this.renderAmount },
  ];

  dialogVisible = false;

  accommodationEdit?: Accommodation;

  @ViewChild('editChargeOp') editChargeOp!: OverlayPanel;

  constructor(
    private route: ActivatedRoute,
    private accommodationsApi: AccommodationsApiService,
    private chargesApi: ChargesApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.fetchAccommodationWithCharges(params['accommodationId']);
    });
  }

  private fetchAccommodationWithCharges(accommodationId: number) {
    this.accommodationsApi
      .getAccommodationWithCharges(accommodationId)
      .subscribe((chargeList) => {
        this.accommodation = chargeList.accommodation;
        this.charges = chargeList.charges;
        this.accommodationEdit = { ...this.accommodation };
        this.backLink = `/students/${this.accommodation?.student?.id}/accommodations`;
      });
  }

  editAccommodation(accommodation: Accommodation) {
    accommodation.student = this.accommodation?.student!;
    accommodation.id = this.accommodation!.id!;
    this.accommodationsApi
      .putAccommodation(accommodation)
      .subscribe(() =>
        this.fetchAccommodationWithCharges(this.accommodation!.id!)
      );
  }

  removeAccommodation() {
    this.accommodationsApi
      .getAccommodationWithCharges(this.accommodation!.id!)
      .subscribe((chargeList) => {
        this.removeAllCharges(chargeList.charges).subscribe((success) => {
          if (success) {
            this.accommodationsApi
              .deleteAccommodation(this.accommodation!.id!)
              .subscribe(() => {
                this.router.navigate([
                  'students',
                  this.accommodation?.student.id,
                  'accommodations',
                ]);
              });
          }
        });
      });
  }

  private removeAllCharges(charges: Charge[]): Observable<boolean> {
    let counter = charges.length;
    if (counter === 0) return new Observable((sub) => sub.next(true));

    const finished = new Subject<boolean>();
    charges.forEach((charge) => {
      this.chargesApi.deleteCharge(charge.id!).subscribe(() => {
        counter--;
        if (counter === 0) {
          finished.next(true);
        }
      });
    });

    return finished.asObservable();
  }

  submitCharge(charge: Charge) {
    charge.accommodation = this.accommodation!.id!;
    this.chargesApi.postCharge(charge).subscribe(() => {
      this.fetchAccommodationWithCharges(this.accommodation!.id!);
      this.dialogVisible = false;
    });
  }

  showDialog() {
    if (this.dialogVisible) return;
    this.dialogVisible = true;
  }

  removeCharge() {
    if (!this.selectedCharge) return;
    this.chargesApi
      .deleteCharge(this.selectedCharge.id!)
      .subscribe(() =>
        this.fetchAccommodationWithCharges(this.accommodation!.id!)
      );
  }

  editCharge(charge: Charge) {
    charge.accommodation = this.accommodation?.id!;
    this.chargesApi
      .putCharge(charge)
      .subscribe(() =>
        this.fetchAccommodationWithCharges(charge.accommodation)
      );
  }

  private renderAmount(charge: Charge) {
    return (charge.amount / 100).toString() + ' PLN';
  }
}
