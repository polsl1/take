import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargesTableComponent } from './charges-table.component';

describe('ChargesTableComponent', () => {
  let component: ChargesTableComponent;
  let fixture: ComponentFixture<ChargesTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChargesTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargesTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
