import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Dialog } from 'primeng/dialog';
import { DataTableColumn } from 'src/app/shared/data-table/data-table.component';
import { StudentFormComponent } from '../shared/student-form/student-form.component';
import { Student } from '../../data-api/students/students.model';
import { StudentsApiService } from 'src/app/data-api/students/students-api.service';

@Component({
  selector: 'dt-student-table',
  templateUrl: './student-table.component.html',
  styleUrls: ['./student-table.component.css'],
})
export class StudentTableComponent implements OnInit {
  students: Student[] = [];
  currentItem: Student | null = null;

  readonly columns: DataTableColumn[] = [
    { field: 'firstName', label: 'First name' },
    { field: 'lastName', label: 'Last name' },
    { field: 'albumNum', label: 'Album number' },
    { field: 'telNum', label: 'Telephone number' },
  ];

  get currentItemId(): string {
    return this.currentItem && this.currentItem.id
      ? this.currentItem.id.toString()
      : '';
  }

  dialogVisible = false;

  @ViewChild('studentForm') studentForm!: StudentFormComponent;
  @ViewChild('studentDialog') studentDialog!: Dialog;

  constructor(
    private studentsApi: StudentsApiService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.fetchStudents();
  }

  private fetchStudents() {
    this.studentsApi.getAllStudents().subscribe((students) => {
      this.students = students;
    });
  }

  onSubmit(student: Student) {
    this.studentsApi.postStudent(student).subscribe(() => {
      this.fetchStudents();
      this.dialogVisible = false;
    });
  }

  showDialog() {
    if (this.dialogVisible) return;
    this.dialogVisible = true;
  }

  onRowSelection(student: Student) {
    if (!student) return;
    this.router.navigate(['students', student.id!, 'accommodations']);
  }
}
