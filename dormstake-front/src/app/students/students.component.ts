import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dt-students',
  template: '<dt-student-table></dt-student-table>'
})
export class StudentsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
