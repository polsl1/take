import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { PanelModule } from 'primeng/panel';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { InputMaskModule } from 'primeng/inputmask';

import { StudentsRoutingModule } from './students-routing.module';
import { StudentTableComponent } from './student-table/student-table.component';
import { SharedModule } from '../shared/shared.module';
import { StudentFormComponent } from './shared/student-form/student-form.component';
import { FormsModule } from '@angular/forms';
import { TooltipModule } from 'primeng/tooltip';
import { ToolbarModule } from 'primeng/toolbar';
import { DividerModule } from 'primeng/divider';
import { ReactiveFormsModule } from '@angular/forms';
import { StudentsComponent } from './students.component';
import { AccommodationTableComponent } from './accommodations-table/accommodations-table.component';
import { AccommodationFormComponent } from './shared/accommodation-form/accommodation-form.component';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { DropdownModule } from 'primeng/dropdown';
import { KeyFilterModule } from 'primeng/keyfilter';
import { ChargeFormComponent } from './shared/charge-form/charge-form.component';
import { ChargesTableComponent } from './charges-table/charges-table.component';

@NgModule({
  declarations: [
    StudentTableComponent,
    StudentFormComponent,
    StudentsComponent,
    AccommodationTableComponent,
    AccommodationFormComponent,
    ChargeFormComponent,
    ChargesTableComponent,
  ],
  imports: [
    CommonModule,
    StudentsRoutingModule,
    ReactiveFormsModule,

    SharedModule,

    TableModule,
    ButtonModule,
    DialogModule,
    PanelModule,
    InputNumberModule,
    InputTextModule,
    InputMaskModule,
    FormsModule,
    TooltipModule,
    ToolbarModule,
    DividerModule,
    OverlayPanelModule,
    DropdownModule,
    KeyFilterModule
  ],
  exports: [StudentsComponent],
})
export class StudentsModule { }
