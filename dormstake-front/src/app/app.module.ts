import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';

import { CardModule } from 'primeng/card';
import { SharedModule } from './shared/shared.module';
import { DataApiModule } from './data-api/data-api.module';
import { DormsModule } from './dorms/dorms.module';
import { StudentsModule } from './students/students.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,

    SharedModule,
    DataApiModule,
    DormsModule,
    StudentsModule,

    CardModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
