package pl.polsl.dormstake.entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "accommodations", uniqueConstraints = {
        @UniqueConstraint(columnNames = { "student_id", "room_id", "date_from" })
})
public class Accommodation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "accommodation_id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "student_id", nullable = false)
    @NotNull
    private Student student;

    @ManyToOne
    @JoinColumn(name = "room_id", nullable = false)
    @NotNull
    private Room room;

    @OneToMany(mappedBy = "accommodation", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    @JsonIgnore
    private List<Charge> charges;

    @Column(nullable = false, name = "date_from")
    @NotNull
    @Temporal(TemporalType.DATE)
    private Date dateFrom;

    @Column(nullable = true)
    @Temporal(TemporalType.DATE)
    private Date dateTo;

    public Accommodation(Long id) {
        this.id = id;
    }

    public Accommodation() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public List<Charge> getCharges() {
        return charges;
    }

    public void setCharges(List<Charge> charges) {
        this.charges = charges;
    }
}
