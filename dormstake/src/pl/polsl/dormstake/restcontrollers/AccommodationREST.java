package pl.polsl.dormstake.restcontrollers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import pl.polsl.dormstake.api.AccommodationAPI;
import pl.polsl.dormstake.ejb.AccommodationEJB;
import pl.polsl.dormstake.entities.Accommodation;
import pl.polsl.dormstake.entities.Charge;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "/accommodations")
@Path("/accommodations")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class AccommodationREST implements AccommodationAPI {

        @EJB
        AccommodationEJB accommodationEJB;

        @Override
        @GET
        @ApiOperation(value = "Get all accommodationso", notes = "A method returning list of accommodations", 
        response = Accommodation.class, responseContainer = "List", nickname = "getAccommodations")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getAccommodations() {
                return Response
                                .status(Response.Status.OK)
                                .entity(this.getAll())
                                .build();
        }

        @Override
        @POST
        @ApiOperation(value = "Create accommodation entity", notes = "A method creating accommodation entity and adding it to database", nickname = "postAcccommodation")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response create(
                        @ApiParam(value = "Accommodation to add to database", required = true, name = "accommodation") Accommodation accommodation) {

                long id = accommodationEJB.create(accommodation).getId();
                URI uri = null;
                try {
                        uri = new URI("accomodations/" + id);
                } catch (Exception ignore) {
                }

                return Response
                                .created(uri)
                                .build();
        }

        @Override
        @GET
        @Path("/{id}")
        @ApiOperation(value = "Get accommodation by id", notes = "A method returning accommodation entity found by given id", response = Accommodation.class, nickname = "getAcccommodationById")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response get(@ApiParam(value = "Accommodation's id", required = true) @PathParam("id") long id) {
                return Response
                                .status(Response.Status.OK)
                                .entity(accommodationEJB.get(Accommodation.class, id))
                                .build();
        }

        @Override
        public List<Accommodation> getAll() {
                return accommodationEJB.getAll(Accommodation.class.getName());
        }

        @Override
        @PUT
        @ApiOperation(value = "Update accommodation", notes = "A method updating an existing accommodation", nickname = "putAcccommodation")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response update(
                        @ApiParam(value = "Accommodation's parameters to update", required = true) Accommodation accommodation) {
                accommodationEJB.update(accommodation);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }

        @Override
        @DELETE
        @Path("/{id}")
        @ApiOperation(value = "Delete accommodation", notes = "A method deleting an existing accommodation", nickname = "deleteAccommodation")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response delete(@ApiParam(value = "Accommodation's id", required = true) @PathParam("id") long id) {
                accommodationEJB.delete(Accommodation.class, id);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }

        @GET
        @Path("/{id}/charges")
        @ApiOperation(value = "Get accommodation specified by id with corresponding charges", response = Charge.class, responseContainer = "Map", nickname = "getAcccommodationWithCharges")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getAccommodationWithCharges(
                        @ApiParam(value = "Accommodation's id", required = true) @PathParam("id") long id) {

                Accommodation accommodation = accommodationEJB.get(Accommodation.class, id);
                Map<String, Object> outMap = new HashMap<String, Object>();
                outMap.put("accommodation", accommodation);
                outMap.put("charges", accommodation.getCharges());

                try {
                        String json = new ObjectMapper().writeValueAsString(outMap);
                        return Response
                                        .status(Response.Status.OK)
                                        .entity(json)
                                        .build();
                } catch (JsonProcessingException e) {
                        return Response.serverError().build();
                }
        }
}
