package pl.polsl.dormstake.restcontrollers;

import io.swagger.annotations.*;
import pl.polsl.dormstake.api.ChargeAPI;
import pl.polsl.dormstake.ejb.ChargeEJB;
import pl.polsl.dormstake.entities.Charge;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.net.URI;
import java.util.List;

@Api(value = "/charges")
@Path("/charges")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class ChargeREST implements ChargeAPI {
        @EJB
        ChargeEJB chargeEJB;

        @Override
        @POST
        @ApiOperation(value = "Create charge entity", notes = "A method creating charge entity and adding it to database", nickname = "postCharge")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response create(
                        @ApiParam(value = "Charge to add to database", required = true, name = "charge") Charge charge) {
                long id = chargeEJB.create(charge).getId();
                URI uri = null;
                try {
                        uri = new URI("charges/" + id);
                } catch (Exception ignore) {
                }

                return Response
                                .created(uri)
                                .build();
        }

        @Override
        @GET
        @Path("/{id}")
        @ApiOperation(value = "Get charge by id", notes = "A method returning charge entity found by given id", response = Charge.class, nickname = "getChargeById")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response get(@ApiParam(value = "Charge's id", required = true) @PathParam("id") long id) {
                return Response
                                .status(Response.Status.OK)
                                .entity(chargeEJB.get(Charge.class, id))
                                .build();
        }

        @Override
        public List<Charge> getAll() {
                return chargeEJB.getAll(Charge.class.getName());
        }

        @GET
        @ApiOperation(value = "Get all charges or the unpaid ones", notes = "A method returning list of charges. If the parameter unpaid is equal to true, then "
                        +
                        "only the unpaid charges are returned, if it's false or undefinied, then all charges " +
                        "are returned", response = Charge.class, responseContainer = "List", nickname = "getCharges")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getCharges(
                        @ApiParam(value = "Is unpaid") @DefaultValue("false") @QueryParam(value = "unpaid") boolean unpaid) {
                return Response
                                .status(Response.Status.OK)
                                .entity(unpaid ? chargeEJB.getUnpaidCharges() : this.getAll())
                                .build();
        }

        @Override
        @PUT
        @ApiOperation(value = "Update charge", notes = "A method updating an existing charge", nickname = "putCharge")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response update(@ApiParam(value = "Charge's parameters to update", required = true) Charge charge) {
                chargeEJB.update(charge);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }

        @Override
        @DELETE
        @Path("/{id}")
        @ApiOperation(value = "Delete charge", notes = "A method deleting an existing charge", nickname = "deleteCharge")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response delete(@ApiParam(value = "Charge's id", required = true) @PathParam("id") long id) {
                chargeEJB.delete(Charge.class, id);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }
}
