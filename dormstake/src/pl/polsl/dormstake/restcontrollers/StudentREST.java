package pl.polsl.dormstake.restcontrollers;

import io.swagger.annotations.*;
import pl.polsl.dormstake.api.StudentAPI;
import pl.polsl.dormstake.ejb.StudentEJB;
import pl.polsl.dormstake.entities.Accommodation;
import pl.polsl.dormstake.entities.Charge;
import pl.polsl.dormstake.entities.Student;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "/students")
@Path("/students")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class StudentREST implements StudentAPI {
        @EJB
        StudentEJB studentEJB;

        @Override
        @POST
        @ApiOperation(value = "Create student entity", notes = "A method creating student entity and adding it to database", nickname = "postStudent")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response create(
                        @ApiParam(value = "Student to add to database", required = true, name = "student") Student student) {
                long id = studentEJB.create(student).getId();
                URI uri = null;
                try {
                        uri = new URI("students/" + id);
                } catch (Exception ignore) {

                        System.out.print(ignore);
                }

                return Response
                                .created(uri)
                                .build();
        }

        @Override
        @GET
        @Path("/{id}")
        @ApiOperation(value = "Get student by id", notes = "A method returning student entity found by given id", response = Student.class, nickname = "getStudentById")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response get(@ApiParam(value = "Student's id", required = true) @PathParam("id") long id) {
                return Response
                                .status(Response.Status.OK)
                                .entity(studentEJB.get(Student.class, id))
                                .build();
        }

        @Override
        public List<Student> getAll() {
                return studentEJB.getAll(Student.class.getName());
        }

        @GET
        @Override
        @ApiOperation(value = "Get students by first name, last name and/or album number or all students", notes = "A method returning list of students that match the given parameters like first name, last name and/or"
                        +
                        "album number, if none of them is given then all students are returned", response = Student.class, responseContainer = "List", nickname = "getStudents")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getStudents(@ApiParam(value = "Student's first name") @QueryParam("firstName") String firstName,
                        @ApiParam(value = "Student's last name") @QueryParam("lastName") String lastName,
                        @ApiParam(value = "Student's album number") @QueryParam("albumNum") String albumNumber) {
                return Response
                                .status(Response.Status.OK)
                                .entity(firstName == null & lastName == null & albumNumber == null ? this.getAll()
                                                : studentEJB.getStudents(firstName, lastName, albumNumber))
                                .build();
        }

        @Override
        @PUT
        @ApiOperation(value = "Update student", notes = "A method updating an existing student", nickname = "putStudent")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response update(@ApiParam(value = "Student's parameters to update", required = true) Student student) {
                studentEJB.update(student);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }

        @Override
        @DELETE
        @Path("/{id}")
        @ApiOperation(value = "Delete student", notes = "A method deleting an existing student", nickname = "deleteStudent")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response delete(@ApiParam(value = "Student's id", required = true) @PathParam("id") long id) {
                studentEJB.delete(Student.class, id);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }

        @Override
        @GET
        @Path("/{id}/charges")
        @ApiOperation(value = "Get student by id with all their charges", notes = "A method returning student entity found by given id with their charges", response = Student.class, nickname = "getStudentWithCharges")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getStudentWithCharges(
                        @ApiParam(value = "Student's id", required = true) @PathParam("id") long id) {

                Student student = studentEJB.get(Student.class, id);
                List<Charge> charges = studentEJB.getStudentCharges(id);
                Map<String, Object> outMap = new HashMap<>();
                outMap.put("student", student);
                outMap.put("charges", charges);

                try {
                        String json = new ObjectMapper().writeValueAsString(outMap);
                        return Response
                                        .ok(json)
                                        .build();
                } catch (JsonProcessingException e) {
                        return Response.serverError().build();
                }
        }

        @GET
        @Path("/{id}/accommodations")
        @ApiOperation(value = "Get student specified by id with corresponding accommoadtions", response = Accommodation.class, responseContainer = "Map", nickname = "getStudentWithAccommodations")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getStudentWithAccommodations(
                        @ApiParam(value = "Student's id", required = true) @PathParam("id") long id) {

                Student student = studentEJB.get(Student.class, id);
                Map<String, Object> outMap = new HashMap<String, Object>();
                outMap.put("student", student);
                outMap.put("accommodations", student.getAccommodations());

                try {
                        String json = new ObjectMapper().writeValueAsString(outMap);
                        return Response
                                        .status(Response.Status.OK)
                                        .entity(json)
                                        .build();
                } catch (JsonProcessingException e) {
                        return Response.serverError().build();
                }
        }
}
