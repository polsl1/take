package pl.polsl.dormstake.restcontrollers;

import io.swagger.annotations.*;
import pl.polsl.dormstake.api.RoomAPI;
import pl.polsl.dormstake.ejb.AccommodationEJB;
import pl.polsl.dormstake.ejb.RoomEJB;
import pl.polsl.dormstake.entities.Charge;
import pl.polsl.dormstake.entities.Room;
import pl.polsl.dormstake.entities.Student;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "/rooms")
@Path("/rooms")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class RoomREST implements RoomAPI {

        @EJB
        RoomEJB roomEJB;

        @EJB
        AccommodationEJB accomEJB;

        @Override
        @POST
        @ApiOperation(value = "Create room entity", notes = "A method creating room entity and adding it to database", nickname = "postRoom")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response create(@ApiParam(value = "Room to add to database", required = true, name = "room") Room room) {
                long id = roomEJB.create(room).getId();
                URI uri = null;
                try {
                        uri = new URI("rooms/" + id);
                } catch (Exception ignore) {
                }

                return Response
                                .created(uri)
                                .build();
        }

        @Override
        @GET
        @Path("/{id}")
        @ApiOperation(value = "Get room by id", notes = "A method returning room entity found by given id", response = Room.class, nickname = "getRoomById")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response get(@ApiParam(value = "Room's id", required = true) @PathParam("id") long id) {
                return Response
                                .status(Response.Status.OK)
                                .entity(roomEJB.get(Room.class, id))
                                .build();
        }

        @Override
        public List<Room> getAll() {
                return roomEJB.getAll(Room.class.getName());
        }

        @Override
        @GET
        @ApiOperation(value = "Get rooms by number, floor, capacity and/or area, dorm", notes = "A method returning list of rooms that match the given parameters like number, floor, capacity, dorm "
                        +
                        "and/or area, if none of them is given then all rooms are returned", response = Room.class, responseContainer = "List", nickname = "getRooms")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getRooms(
                        @ApiParam(value = "Room's number") @DefaultValue("-1") @QueryParam("number") int number,
                        @ApiParam(value = "Room's floor") @DefaultValue("-1") @QueryParam("floor") int floor,
                        @ApiParam(value = "Room's capacity") @DefaultValue("-1") @QueryParam("capacity") int capacity,
                        @ApiParam(value = "Room's area") @DefaultValue("-1") @QueryParam("area") int area,
                        @ApiParam(value = "Room's dorm") @DefaultValue("-1") @QueryParam("dormId") int dormId) {
                return Response
                                .status(Response.Status.OK)
                                .entity(number == -1 & floor == -1 & capacity == -1 & area == -1 & dormId == -1
                                                ? this.getAll()
                                                : roomEJB.getRooms(number, floor, capacity, area, dormId))
                                .build();
        }

        @Override
        @PUT
        @ApiOperation(value = "Update room", notes = "A method updating an existing room", nickname = "putRoom")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response update(@ApiParam(value = "Room's parameters to update", required = true) Room room) {
                roomEJB.update(room);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }

        @Override
        @DELETE
        @Path("/{id}")
        @ApiOperation(value = "Delete room", notes = "A method deleting an existing room", nickname = "deleteRoom")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response delete(@ApiParam(value = "Room's id", required = true) @PathParam("id") long id) {
                roomEJB.delete(Room.class, id);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }

        @Override
        @GET
        @Path("/{id}/students")
        @ApiOperation(value = "Get students from room identified by id and the room itself", notes = "A method returning students currently living"
                        + "in the specified room with the room object", nickname = "getStudentsWithRoom")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getStudentsWithRoom(@ApiParam(value = "Room's id", required = true) @PathParam("id") long id) {

                Room room = roomEJB.get(Room.class, id);
                List<Student> students = accomEJB.getStudentsFromRoom(id);
                Map<String, Object> outMap = new HashMap<>();
                outMap.put("room", room);
                outMap.put("students", students);

                try {
                        String json = new ObjectMapper().writeValueAsString(outMap);
                        return Response
                                        .ok(json)
                                        .build();
                } catch (JsonProcessingException e) {
                        return Response.serverError().build();
                }
        }

        @Override
        @GET()
        @Path("/{id}/charges")
        @ApiOperation(value = "Get charges from room identified by id and the room itself", notes = "A method returning charges created "
                        + "for the specified room with the room object", nickname = "getChargesWithRoom")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getChargesWithRoom(@ApiParam(value = "Room's id", required = true) @PathParam("id") long id,
                        @ApiParam(value = "Student ID to limit the result") @QueryParam("studentId") Long studentId) {

                Room room = roomEJB.get(Room.class, id);
                List<Charge> charges = accomEJB.getChargesFromRoom(id, studentId);
                Map<String, Object> outMap = new HashMap<>();
                outMap.put("room", room);
                outMap.put("charges", charges);

                try {
                        String json = new ObjectMapper().writeValueAsString(outMap);
                        return Response
                                        .ok(json)
                                        .build();
                } catch (JsonProcessingException e) {
                        return Response.serverError().build();
                }
        }
}
