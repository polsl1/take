package pl.polsl.dormstake.api;

import pl.polsl.dormstake.entities.Room;

import javax.ejb.Local;
import javax.ws.rs.core.Response;

@Local
public interface RoomAPI extends DormstakeAPI<Room> {
    Response getRooms(int number, int floor, int capacity, int area, int dormId);

    Response getStudentsWithRoom(long id);

    Response getChargesWithRoom(long id, Long studentId);
}
