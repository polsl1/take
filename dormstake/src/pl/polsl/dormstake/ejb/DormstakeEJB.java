package pl.polsl.dormstake.ejb;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;

@Stateless
public abstract class DormstakeEJB<T extends Object> {
    @PersistenceContext(name = "dormstake")
    EntityManager manager;

    @Inject
    Validator validator;

    public T create(T entity) {
        validateEntity(entity);
        manager.persist(entity);
        return entity;
    }

    public T get(Class<T> entityClass, long id) {
        return manager.find(entityClass, id);
    }

    public List<T> getAll(String entityName) {
        Query query = manager.createQuery("SELECT e FROM " + entityName + " e");
        return query.getResultList();
    }

    public void update(T entity) {
        validateEntity(entity);
        entity = manager.merge(entity);
    }

    public void delete(Class<T> entityClass, long id) {
        T entity = manager.find(entityClass, id);
        if (entity != null)
            manager.remove(entity);
    }

    public void validateEntity(T entity) {
        Set<ConstraintViolation<T>> violations = validator.validate(entity);
        if (!violations.isEmpty())
            throw new ConstraintViolationException(violations);
    }
}
