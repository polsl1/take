package pl.polsl.dormstake.ejb;

import pl.polsl.dormstake.entities.Room;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class RoomEJB extends DormstakeEJB<Room> {
    @PersistenceContext(name = "dormstake")
    EntityManager manager;

    public List<Room> getRooms(int number, int floor, int capacity, int area, int dormId) {
        String qString = "SELECT r FROM Room r JOIN r.dorm d WHERE (:number = -1 OR r.number = :number) AND " +
                "(:floor = -1 OR r.floor = :floor) AND (:capacity = -1 OR r.capacity = :capacity) AND " +
                "(:area = -1 OR r.area = :area) AND (:dormId = -1 OR d.id = :dormId)";
        TypedQuery<Room> query = manager.createQuery(qString, Room.class);
        query.setParameter("number", number);
        query.setParameter("floor", floor);
        query.setParameter("capacity", capacity);
        query.setParameter("area", area);
        query.setParameter("dormId", dormId);
        return query.getResultList();

    }
}
