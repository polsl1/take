package pl.polsl.dormstake.ejb;

import pl.polsl.dormstake.entities.Charge;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.List;

@Stateless
public class ChargeEJB extends DormstakeEJB<Charge> {
    @PersistenceContext(name = "dormstake")
    EntityManager manager;

    public List<Charge> getUnpaidCharges() {
        TypedQuery<Charge> query = manager.createQuery("SELECT c FROM Charge c WHERE c.dueDate < CURRENT_DATE AND " +
                "c.paidDate IS NULL", Charge.class);
        return query.getResultList();
    }
}
