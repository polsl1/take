package pl.polsl.dormstake.ejb;

import pl.polsl.dormstake.entities.Dorm;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.List;

@Stateless
public class DormEJB extends DormstakeEJB<Dorm> {
    @PersistenceContext(name = "dormstake")
    EntityManager manager;

    public List<Dorm> getDorms(String name, String city) {
        TypedQuery<Dorm> query = manager
                .createQuery("SELECT d FROM Dorm d WHERE (:name IS NULL OR d.name = :name) AND " +
                        "(:city IS NULL OR d.city = :city)", Dorm.class);
        query.setParameter("name", name);
        query.setParameter("city", city);
        return query.getResultList();
    }
}
