package pl.polsl.dormstake.api;

import pl.polsl.dormstake.entities.Charge;

import javax.ejb.Local;

@Local
public interface ChargeAPI extends DormstakeAPI<Charge> {

}
