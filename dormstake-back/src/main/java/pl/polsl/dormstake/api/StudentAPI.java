package pl.polsl.dormstake.api;

import pl.polsl.dormstake.entities.Student;

import javax.ejb.Local;
import javax.ws.rs.core.Response;

@Local
public interface StudentAPI extends DormstakeAPI<Student> {
    Response getStudents(String firstName, String lastName, String albumNumber);

    Response getStudentWithCharges(long id);

    Response getStudentWithAccommodations(long id);
}
