package pl.polsl.dormstake.api;

import pl.polsl.dormstake.entities.Accommodation;

import javax.ejb.Local;
import javax.ws.rs.core.Response;

@Local
public interface AccommodationAPI extends DormstakeAPI<Accommodation> {
    Response getAccommodations();
    Response getAccommodationWithCharges(long id);
}
