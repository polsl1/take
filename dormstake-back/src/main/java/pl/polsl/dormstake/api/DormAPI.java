package pl.polsl.dormstake.api;

import pl.polsl.dormstake.entities.Dorm;

import javax.ejb.Local;
import javax.ws.rs.core.Response;

@Local
public interface DormAPI extends DormstakeAPI<Dorm> {
    Response getDorms(String name, String city);

    Response getDormWithRooms(long id);
}
