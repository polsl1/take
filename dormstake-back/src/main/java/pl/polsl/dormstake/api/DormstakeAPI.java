package pl.polsl.dormstake.api;

import javax.ejb.Local;
import javax.ws.rs.core.Response;
import java.util.List;

@Local
public interface DormstakeAPI<T> {
    Response create(T t);
    Response get(long id);
    List<T> getAll();
    Response update(T t);
    Response delete(long id);
}
