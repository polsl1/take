package pl.polsl.dormstake.ejb;

import pl.polsl.dormstake.entities.Accommodation;
import pl.polsl.dormstake.entities.Charge;
import pl.polsl.dormstake.entities.Room;
import pl.polsl.dormstake.entities.Student;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.Date;
import java.util.List;

@Stateless
public class AccommodationEJB extends DormstakeEJB<Accommodation> {
    @PersistenceContext(name = "dormstake")
    EntityManager manager;

    public List<Student> getStudentsFromRoom(long roomId) {
        String qString = "SELECT s FROM Room r JOIN r.accommodations a JOIN a.student s " +
                "WHERE r.id = :roomId AND a.dateFrom < CURRENT_DATE() AND (a.dateTo IS NULL OR a.dateTo > CURRENT_DATE())";
        TypedQuery<Student> query = manager.createQuery(qString, Student.class);
        query.setParameter("roomId", roomId);

        return query.getResultList();
    }

    public List<Charge> getChargesFromRoom(long roomId, Long studentId) {
        TypedQuery<Charge> query = null;

        if (studentId == null) {
            String qString = "SELECT c FROM Charge c JOIN c.accommodation a JOIN a.room r " +
                    "WHERE r.id = :roomId";
            query = manager.createQuery(qString, Charge.class);
            query.setParameter("roomId", roomId);
        } else {
            String qString = "SELECT c FROM Charge c JOIN c.accommodation a JOIN a.room r JOIN a.student s " +
                    "WHERE r.id = :roomId AND s.id = :studentId";
            query = manager.createQuery(qString, Charge.class);
            query.setParameter("roomId", roomId).setParameter("studentId", studentId);
        }

        return query.getResultList();
    }

    public Room getRoomForStudent(long studentId) {
        String qString = "SELECT r Accomodation a JOIN a.room r JOIN a.student s " +
                "WHERE s.id = :studentId AND a.dateTo IS NULL";
        TypedQuery<Room> query = manager.createQuery(qString, Room.class);
        query.setParameter("studentId", studentId);

        return query.getSingleResult();
    }
}
