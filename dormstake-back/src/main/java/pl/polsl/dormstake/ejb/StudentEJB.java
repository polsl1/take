package pl.polsl.dormstake.ejb;

import pl.polsl.dormstake.entities.Accommodation;
import pl.polsl.dormstake.entities.Charge;
import pl.polsl.dormstake.entities.Student;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import java.util.List;

@Stateless
public class StudentEJB extends DormstakeEJB<Student> {

    @PersistenceContext(name = "dormstake")
    EntityManager manager;

    public List<Student> getStudents(String firstName, String lastName, String albumNumber) {
        TypedQuery<Student> query = manager
                .createQuery("SELECT s FROM Student s WHERE (:firstName IS NULL OR s.firstName = " +
                        ":firstName) AND (:lastName IS NULL OR s.lastName = :lastName) AND (:albumNumber IS NULL OR " +
                        "s.albumNum = :albumNumber)", Student.class);
        query.setParameter("firstName", firstName);
        query.setParameter("lastName", lastName);
        query.setParameter("albumNumber", albumNumber);
        return query.getResultList();
    }

    public List<Charge> getStudentCharges(long id) {
        String qString = "SELECT c FROM Charge c JOIN c.accommodation a JOIN a.student s " +
                "WHERE s.id = :studentId";
        TypedQuery<Charge> query = manager.createQuery(qString, Charge.class);
        query.setParameter("studentId", id);

        return query.getResultList();
    }

    public List<Accommodation> getStudentAccommodations(long id) {
        String qString = "SELECT a FROM Accommodation a JOIN a.student s " +
                "WHERE s.id = :studentId";
        TypedQuery<Accommodation> query = manager.createQuery(qString, Accommodation.class);
        query.setParameter("studentId", id);

        return query.getResultList();
    }
}
