package pl.polsl.dormstake.restcontrollers;

import io.swagger.annotations.*;
import pl.polsl.dormstake.api.DormAPI;
import pl.polsl.dormstake.ejb.DormEJB;
import pl.polsl.dormstake.entities.Dorm;
import pl.polsl.dormstake.entities.Room;

import javax.ejb.EJB;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api(value = "/dorms")
@Path("/dorms")
@Consumes({ "application/json" })
@Produces({ "application/json" })
public class DormREST implements DormAPI {

        @EJB
        DormEJB dormEJB;

        @Override
        @GET
        @ApiOperation(value = "Get dorms by name and/or city", notes = "A method returning list of dorms that match the given parameters like city and/or name, "
                        +
                        "if none of them is given then all dorms are returned", response = Dorm.class, responseContainer = "List", nickname = "getDorms")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getDorms(@ApiParam(value = "Dorm's name") @QueryParam("name") String name,
                        @ApiParam(value = "Dorm's city") @QueryParam("city") String city) {
                return Response
                                .status(Response.Status.OK)
                                .entity(name == null & city == null ? this.getAll() : dormEJB.getDorms(name, city))
                                .build();
        }

        @Override
        public List<Dorm> getAll() {
                return dormEJB.getAll(Dorm.class.getName());
        }

        @Override
        @POST
        @ApiOperation(value = "Create dorm entity", notes = "A method creating dorm entity and adding it to database", nickname = "postDorm")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response create(@ApiParam(value = "Dorm to add to database", required = true, name = "dorm") Dorm dorm) {
                long id = dormEJB.create(dorm).getId();
                URI uri = null;
                try {
                        uri = new URI("dorms/" + id);
                } catch (Exception ignore) {
                }

                return Response
                                .created(uri)
                                .build();
        }

        @Override
        @GET
        @Path("/{id}")
        @ApiOperation(value = "Get dorm by id", notes = "A method returning dorm entity found by given id", response = Room.class, nickname = "getDormById")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response get(@ApiParam(value = "Dorm's id", required = true) @PathParam("id") long id) {
                return Response
                                .status(Response.Status.OK)
                                .entity(dormEJB.get(Dorm.class, id))
                                .build();
        }

        @Override
        @PUT
        @ApiOperation(value = "Update dorm", notes = "A method updating an existing dorm", nickname = "putDorm")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response update(@ApiParam(value = "Dorm's parameters to update", required = true) Dorm dorm) {
                dormEJB.update(dorm);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }

        @Override
        @DELETE
        @Path("/{id}")
        @ApiOperation(value = "Delete dorm", notes = "A method deleting an existing dorm", nickname = "deleteDorm")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response delete(@ApiParam(value = "Dorm's id", required = true) @PathParam("id") long id) {
                dormEJB.delete(Dorm.class, id);
                return Response
                                .status(Response.Status.OK)
                                .build();
        }

        @Override
        @GET
        @Path("/{id}/rooms")
        @ApiOperation(value = "Get dorm specified by id with corresponding rooms", response = Room.class, responseContainer = "Map", nickname = "getDormWithRooms")
        @ApiResponses(value = { @ApiResponse(code = 200, message = "Success"),
                        @ApiResponse(code = 500, message = "Internal server error"),
                        @ApiResponse(code = 404, message = "Table not found") })
        public Response getDormWithRooms(@ApiParam(value = "Dorm's id", required = true) @PathParam("id") long id) {

                Dorm dorm = dormEJB.get(Dorm.class, id);
                Map<String, Object> outMap = new HashMap<String, Object>();
                outMap.put("dorm", dorm);
                outMap.put("rooms", dorm.getRooms());

                try {
                        String json = new ObjectMapper().writeValueAsString(outMap);
                        return Response
                                        .status(Response.Status.OK)
                                        .entity(json)
                                        .build();
                } catch (JsonProcessingException e) {
                        return Response.serverError().build();
                }
        }
}
