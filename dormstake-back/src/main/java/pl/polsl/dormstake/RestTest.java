package pl.polsl.dormstake;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

@Path("/test")
public class RestTest {

    @GET
    public String test() {
        return "TEST";
    }

    @GET
    @Path("hello")
    public String testHello() {
        return "Hello world";
    }
}
